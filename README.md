[![Prodkt Colors Logo](colors.png)](https://prodkt.cloud/colors)

# Prodkt Colors

**A gorgeous, accessible color system.**

---

## Documentation

For full documentation, visit [prodkt.cloud/colors/docs](https://prodkt.cloud/colors/docs).

## Installation

`yarn add @prodkt/colors`

## Authors

- Bryan Funk ([@prodkt](https://twitter.com/prodkt_))